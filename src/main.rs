use termion::raw::IntoRawMode;
use termion::event::Key;
use termion::input::TermRead;
use once_cell::sync::Lazy;
use std::sync::Mutex;
use std::io::{ Write, stdin, stdout };

enum Views {
    TODO,
    DONE,
}

enum Moves {
    UP,
    DOWN
}

// Config
struct Config {
    status: Views,
    status_msg: String,
    todo: Vec<String>,
    done: Vec<String>,
    pos: usize,
}

impl Config {
    fn new() -> Config {
        Config {
            status: Views::TODO,
            status_msg: String::new(),
            pos: 0,
            todo: Vec::new(),
            done: Vec::new(),
        }
    }
}

static CONFIG: Lazy<Mutex<Config>> = Lazy::new(|| Mutex::new(Config::new()));

//Navigation
fn select(dir: Moves) {
    let cfg = &mut CONFIG.lock().unwrap();

    let size = match cfg.status {
        Views::TODO => cfg.todo.len(),
        Views::DONE => cfg.done.len(),
    };
    
    match dir {
        Moves::UP => if cfg.pos != 0 { cfg.pos -= 1 },
        Moves::DOWN => if cfg.pos < size - 1 { cfg.pos += 1 },
    }
}

// Output
fn buff_push(buff: &mut String, s: &str) {
    buff.push_str(s);
}

fn status_message(msg: &str) {
    let cfg = &mut CONFIG.lock().unwrap();
    cfg.status_msg = msg.to_string();
}

fn draw_status(buff: &mut String) {
    let view_name = match CONFIG.lock().unwrap().status {
        Views::TODO => "TODO",
        Views::DONE => "DONE"
    };

    buff_push(buff, format!("\x1b[1m\x1b[34m{}\x1b[0m {}",view_name, CONFIG.lock().unwrap().status_msg).as_str());
    buff_push(buff, "\r\n-------------------\r\n"); 
}

fn draw_content(buff: &mut String) {
    let cfg = &mut CONFIG.lock().unwrap();

    let content = match cfg.status {
        Views::TODO => &cfg.todo,
        Views::DONE => &cfg.done,
    };

    for (i, row) in content.iter().enumerate() {
        if i == cfg.pos.into() { 
            buff_push(buff, "\x1b[34m\x1b[1m->");
        }

        let r = format!(" {}\r\n", row);
        buff_push(buff, r.as_str());
        buff_push(buff, "\x1b[0m");
    }
}

fn draw() {
    let mut buff = String::new();
    buff_push(&mut buff, "\x1b[2J\x1b[1;1H");
    draw_status(&mut buff);
    draw_content(&mut buff);

    write!(stdout(), "{}", buff).unwrap();
}

//Input
fn prompt(label: &str) -> Result<String, std::io::Error> {
    let stdin = stdin();
    let mut buff = String::from("");
    status_message(format!("{}: {}", label, buff).as_str());
    draw();

    for c in stdin.keys() {
        match c? {
            Key::Char('\n') => {
                status_message("Added new task!");
                break
            },
            Key::Char(ch) => buff.push(ch),
            Key::Backspace => { buff.pop(); },
            Key::Esc => { 
                status_message("Aborted!");
                return Err(std::io::ErrorKind::Interrupted.into());
            },
            _ => (),
        }
        
        status_message(format!("{}: {}", label, buff).as_str());
        draw();
        stdout().flush()?;
    }

    Ok(buff)
}

fn mark_as_done(remove: bool) {
    let cfg = &mut CONFIG.lock().unwrap();
    let task_index = cfg.pos;
    let task = cfg.todo.remove(task_index);

    if !remove {
        cfg.done.push(task);
    }
}

fn main() -> Result<(), std::io::Error> {
    let stdin = stdin();
    let mut stdout = stdout().into_raw_mode()?;
    write!(stdout, "{}{}", termion::clear::All, termion::cursor::Hide)?;
    draw();
    stdout.flush().unwrap();

    for c in stdin.keys() {
        match c.unwrap() {
            Key::Char('j') => select(Moves::DOWN),
            Key::Char('k') => select(Moves::UP),
            Key::Char('n') => { 
                match prompt("Enter task") {
                    Ok(t) => CONFIG.lock().unwrap().todo.push(t),
                    _ => (),
                }
            },
            Key::Char('m') => { 
                mark_as_done(false);
                status_message("Moved to done");
            },
            Key::Char('r') => { 
                mark_as_done(true);
                status_message("Removed!");
            },
            Key::Char('s') => { 
                CONFIG.lock().unwrap().status = Views::TODO;
                CONFIG.lock().unwrap().pos = 0;
            }
            Key::Char('d') => { 
                CONFIG.lock().unwrap().status = Views::DONE;
                CONFIG.lock().unwrap().pos = 0;
            }
            
            Key::Ctrl('q') => break,
            _ => ()
        }

        draw();
        stdout.flush().unwrap();
    }

    write!(stdout, "{}", termion::cursor::Show)?;

    Ok(())
}
