![Screen1](./screen1.png)
![Screen1](./screen2.png)

# Usage

    cargo run

# Controls

|Keys|Description|
|---|---|
|<kbd>k</kbd>, <kbd>j</kbd>|Move cursor up and down|
|<kbd>Ctrl + q</kbd>|Quit|
|<kbd>r</kbd>|Remove current item|
|<kbd>n</kbd>|Insert a new item|
|<kbd>m</kbd>|Mark current item as done|
|<kbd>s</kbd>|Switch to the TODO view|
|<kbd>d</kbd>|Switch to the DONE view|
